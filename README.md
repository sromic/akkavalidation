# README #

## For running this project need to:
* SBT installed
* java JRE/JDK installed
* install Ebean enhancement https://www.youtube.com/watch?v=o4kmglM48Vc&feature=youtu.be
* MySQL database running and
* in ebean.properties (which is under src/resources/ folder) file change MySQL database where to connect
* no need for table creation, ebean will take care of that ( you can prevent this feature of auto creation setting property in ebean.properties file ebean.ddl.generate=false


## After above requirements are satisfied, then run:
* sbt assembly - it should create executable JAR file in project_dir/target/scala-2.xx/akkaValidation.jar
* java -jar akkaValidation.jar