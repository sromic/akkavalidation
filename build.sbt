name := "akkaValidation"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++=  Seq(
	"com.typesafe.akka" % "akka-actor_2.11" % "2.4.9-RC2",
	"mysql" % "mysql-connector-java" % "5.1.39",
	"org.avaje.ebean" % "ebean" % "8.2.1",
	"com.google.inject" % "guice" % "4.1.0")

mainClass in assembly := Some("com.upwork.akka.validation.Boot")
assemblyJarName in assembly := "akkaValidation.jar"
