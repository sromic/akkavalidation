package com.upwork.akka.validation.injector;

import com.avaje.ebean.EbeanServer;
import com.google.inject.AbstractModule;
import com.upwork.akka.validation.provider.EbeanServerProvider;
import com.upwork.akka.validation.repositories.LogRepository;
import com.upwork.akka.validation.repositories.impl.LogRepositoryImpl;
import com.upwork.akka.validation.utils.converter.ClientTimeoutConverter;
import com.upwork.akka.validation.utils.converter.ModelsConverter;
import com.upwork.akka.validation.utils.converter.impl.ClientTimeoutConverterImpl;
import com.upwork.akka.validation.utils.converter.impl.ModelsConverterImpl;

/**
 * Created by sromic on 18/08/16.
 */
public final class ComponentInjector extends AbstractModule {
    @Override
    protected void configure() {
        bind(EbeanServer.class).toProvider(EbeanServerProvider.class).asEagerSingleton();
        bind(LogRepository.class).to(LogRepositoryImpl.class);
        bind(ModelsConverter.class).to(ModelsConverterImpl.class);
        bind(ClientTimeoutConverter.class).to(ClientTimeoutConverterImpl.class);
    }
}
