package com.upwork.akka.validation.models;

import com.avaje.ebean.Model;

import javax.persistence.*;

/**
 * Created by sromic on 25/08/16.
 */
@Entity
@Table(name = "log")
public final class Log extends Model {

    public static final Finder<Long, Log> finder = new Finder<Long, Log>(Log.class);

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "response_duration")
    private Integer responseDuration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Integer getResponseDuration() {
        return responseDuration;
    }

    public void setResponseDuration(Integer responseDuration) {
        this.responseDuration = responseDuration;
    }

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", responseDuration=" + responseDuration +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Log log = (Log) o;

        if (id != null ? !id.equals(log.id) : log.id != null) return false;
        if (clientId != null ? !clientId.equals(log.clientId) : log.clientId != null) return false;
        return responseDuration != null ? responseDuration.equals(log.responseDuration) : log.responseDuration == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (responseDuration != null ? responseDuration.hashCode() : 0);
        return result;
    }
}
