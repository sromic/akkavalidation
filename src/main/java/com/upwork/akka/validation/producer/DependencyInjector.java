package com.upwork.akka.validation.producer;

import akka.actor.Actor;
import akka.actor.IndirectActorProducer;
import com.google.inject.Injector;

/**
 * Created by sromic on 18/08/16.
 */
public final class DependencyInjector implements IndirectActorProducer {

    final Injector injector;

    final Class<? extends Actor> actorClass;

    public DependencyInjector(final Injector injector, final Class<? extends Actor> actorClass) {
        this.injector = injector;
        this.actorClass = actorClass;
    }

    @Override
    public Actor produce() {
        return injector.getInstance(actorClass);
    }

    @Override
    public Class<? extends Actor> actorClass() {
        return actorClass;
    }
}
