package com.upwork.akka.validation.actors;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import scala.concurrent.duration.Duration;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by sromic on 25/08/16.
 */
@Singleton
public final class ServerActor extends AbstractLoggingActor {

    private final ActorRef performanceActor;

    @Inject
    public ServerActor(ActorRef performanceActor) {
        this.performanceActor = performanceActor;

        receive(ReceiveBuilder
            .match(Event.class, e -> {
                log().info("message received: {}", e);

                final int calculation = new Random().nextInt(500);
                log().info("sending response to client");
                getContext().system().scheduler()
                        .scheduleOnce(
                                Duration.create(calculation, TimeUnit.MILLISECONDS),
                                sender(),
                                ClientActor.SimpleResponse.createInstance(calculation),
                                getContext().dispatcher(),
                                self()
                        );

                //sender().tell(ClientActor.SimpleResponse.createInstance(calculation), self());

                log().info("sending processing duration to performanceActor");
                performanceActor.tell(PerformanceActor.ProcessingDuration.createInstance(calculation, e.getClientId()), self());
            })
                .build()
        );
    }

    @Inject
    public static Props props(final ActorRef performanceActor) {
        return Props.create(ServerActor.class, performanceActor);
    }


    public static final class Event {
        private final String clientId;

        private Event(String clientId) {
            this.clientId = clientId;
        }

        public static Event createInstance(final String clientId) {
            return new Event(clientId);
        }

        public String getClientId() {
            return clientId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Event event = (Event) o;

            return clientId != null ? clientId.equals(event.clientId) : event.clientId == null;

        }

        @Override
        public int hashCode() {
            return clientId != null ? clientId.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "Event{" +
                    "clientId='" + clientId + '\'' +
                    '}';
        }
    }
}
