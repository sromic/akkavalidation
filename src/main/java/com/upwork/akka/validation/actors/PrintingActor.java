package com.upwork.akka.validation.actors;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.upwork.akka.validation.repositories.LogRepository;

/**
 * Created by sromic on 02/09/16.
 */
@Singleton
public final class PrintingActor extends AbstractLoggingActor {

    @Inject
    private LogRepository logRepository;

    public PrintingActor() {
        receive(ReceiveBuilder
                .match(Print.class, p -> {
                    logRepository.getAll().forEach(System.out::println);
                } )
                .build());
    }

    public enum Print {
        START
    }

    public static final Props props() {
        return Props.create(PrintingActor.class);
    }
}
