package com.upwork.akka.validation.actors;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.upwork.akka.validation.models.Log;
import com.upwork.akka.validation.repositories.LogRepository;
import com.upwork.akka.validation.utils.converter.ClientTimeoutConverter;
import com.upwork.akka.validation.utils.converter.ModelsConverter;

/**
 * Created by sromic on 25/08/16.
 */
@Singleton
public final class PerformanceActor extends AbstractLoggingActor {

    @Inject
    private LogRepository logRepository;

    @Inject
    private ModelsConverter modelsConverter;

    @Inject
    private ClientTimeoutConverter clientTimeoutConverter;

    //TODO think about actor 'pool' as db connection pool for inserting records into db
    public PerformanceActor() {
        receive(ReceiveBuilder
                .match(ProcessingDuration.class, processingDuration -> {
                    log().info("received message: {}", processingDuration);

                    final Log log = modelsConverter.convert(processingDuration);
                    logRepository.insert(log);
                })
                .match(ClientTimedOut.class, clientTimedOut -> {
                    log().info("received client: {} timeout exception: {}", sender(), clientTimedOut);

                    final Log log = clientTimeoutConverter.convert(clientTimedOut);
                    logRepository.insert(log);
                })
                .build()
        );
    }

    public static class ClientTimedOut {
        private final int processDuration;
        private final String requestId;

        private ClientTimedOut(int processDuration, String requestId) {
            this.processDuration = processDuration;
            this.requestId = requestId;
        }

        public static ClientTimedOut createInstance(int processDuration, String requestId) {
            return new ClientTimedOut(processDuration, requestId);
        }

        public int getProcessDuration() {
            return processDuration;
        }

        public String getRequestId() {
            return requestId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ClientTimedOut that = (ClientTimedOut) o;

            if (processDuration != that.processDuration) return false;
            return requestId != null ? requestId.equals(that.requestId) : that.requestId == null;

        }

        @Override
        public int hashCode() {
            int result = processDuration;
            result = 31 * result + (requestId != null ? requestId.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "ClientTimedOut{" +
                    "processDuration=" + processDuration +
                    ", requestId='" + requestId + '\'' +
                    '}';
        }
    }

    public static class ProcessingDuration {
        private final int processDuration;
        private final String requestId;

        private ProcessingDuration(int processDuration, String requestId) {
            this.processDuration = processDuration;
            this.requestId = requestId;
        }

        public static ProcessingDuration createInstance(final int processDuration, final String requestId) {
            return new ProcessingDuration(processDuration, requestId);
        }

        public int getProcessDuration() {
            return processDuration;
        }

        public String getRequestId() {
            return requestId;
        }

        @Override
        public String toString() {
            return "ProcessingDuration{" +
                    "processDuration=" + processDuration +
                    ", requestId='" + requestId + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProcessingDuration that = (ProcessingDuration) o;

            if (processDuration != that.processDuration) return false;
            return requestId != null ? requestId.equals(that.requestId) : that.requestId == null;

        }

        @Override
        public int hashCode() {
            int result = processDuration;
            result = 31 * result + (requestId != null ? requestId.hashCode() : 0);
            return result;
        }
    }

    public static final Props props() {
        return Props.create(PerformanceActor.class, PerformanceActor::new);
    }

    public LogRepository getLogRepository() {
        return logRepository;
    }

    public void setLogRepository(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public ModelsConverter getModelsConverter() {
        return modelsConverter;
    }

    public void setModelsConverter(ModelsConverter modelsConverter) {
        this.modelsConverter = modelsConverter;
    }

    public ClientTimeoutConverter getClientTimeoutConverter() {
        return clientTimeoutConverter;
    }

    public void setClientTimeoutConverter(ClientTimeoutConverter clientTimeoutConverter) {
        this.clientTimeoutConverter = clientTimeoutConverter;
    }
}
