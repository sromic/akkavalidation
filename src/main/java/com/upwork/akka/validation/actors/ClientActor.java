package com.upwork.akka.validation.actors;

import akka.actor.*;
import akka.japi.pf.ReceiveBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.upwork.akka.validation.utils.Messages;
import scala.concurrent.duration.Duration;

/**
 * Created by sromic on 25/08/16.
 */
@Singleton
public final class ClientActor extends AbstractLoggingActor {

    private final ActorRef performanceActor;

    @Inject
    public ClientActor(ActorRef performanceActor) {
        this.performanceActor = performanceActor;

        getContext().setReceiveTimeout(Duration.create("1 seconds"));

        receive(ReceiveBuilder
                .match(SimpleResponse.class, simpleResponse -> {
                    log().info("received response: {} from serverActor", simpleResponse);
                    //after response is received kill myself
                    self().tell(Kill.getInstance(), ActorRef.noSender());
                })
                .match(ReceiveTimeout.class, receiveTimeout -> {
                    log().info("received timeout: {}", receiveTimeout);
                    performanceActor.tell(PerformanceActor.ClientTimedOut
                            .createInstance(Messages.CLIENT_TIMEDOUT.getStatus(), self().path().name()), self());

                    //turn of receive timeout period
                    getContext().setReceiveTimeout(Duration.Undefined());

                    //after response is received kill myself
                    self().tell(Kill.getInstance(), ActorRef.noSender());
                })
                .build()
        );
    }

    public ActorRef getPerformanceActor() {
        return performanceActor;
    }

    public static class SimpleResponse {
        private final int responseDuration;

        private SimpleResponse(int responseDuration) {
            this.responseDuration = responseDuration;
        }

        public int getResponseDuration() {
            return responseDuration;
        }

        public static SimpleResponse createInstance(final int responseDuration) {
            return new SimpleResponse(responseDuration);
        }

        @Override
        public String toString() {
            return "SimpleResponse{" +
                    "responseDuration=" + responseDuration +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SimpleResponse that = (SimpleResponse) o;

            return responseDuration == that.responseDuration;

        }

        @Override
        public int hashCode() {
            return responseDuration;
        }
    }

    @Inject
    public static final Props props(final ActorRef performanceActor) {
        return Props.create(ClientActor.class, performanceActor);
    }
}
