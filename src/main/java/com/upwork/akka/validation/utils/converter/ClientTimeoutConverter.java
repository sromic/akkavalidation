package com.upwork.akka.validation.utils.converter;

import com.upwork.akka.validation.actors.PerformanceActor;
import com.upwork.akka.validation.models.Log;

/**
 * Created by sromic on 01/09/16.
 */
public interface ClientTimeoutConverter extends Converter<PerformanceActor.ClientTimedOut, Log> {
}
