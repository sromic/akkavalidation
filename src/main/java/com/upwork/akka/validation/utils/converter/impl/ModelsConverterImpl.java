package com.upwork.akka.validation.utils.converter.impl;

import com.google.inject.Singleton;
import com.upwork.akka.validation.actors.PerformanceActor;
import com.upwork.akka.validation.models.Log;
import com.upwork.akka.validation.utils.converter.ModelsConverter;

/**
 * Created by sromic on 01/09/16.
 */
@Singleton
public class ModelsConverterImpl implements ModelsConverter {
    @Override
    public Log convert(final PerformanceActor.ProcessingDuration processingDuration) {
        final Log log = new Log();
        log.setClientId(processingDuration.getRequestId());
        log.setResponseDuration(processingDuration.getProcessDuration());

        return log;
    }
}
