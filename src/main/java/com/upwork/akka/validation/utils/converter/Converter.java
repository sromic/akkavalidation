package com.upwork.akka.validation.utils.converter;

/**
 * Created by sromic on 01/09/16.
 */
public interface Converter<F, T> {
    T convert(F f);
}
