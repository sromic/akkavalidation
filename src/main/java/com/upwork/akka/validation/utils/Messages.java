package com.upwork.akka.validation.utils;

/**
 * Created by sromic on 01/09/16.
 */
public enum Messages {
    CLIENT_TIMEDOUT(-1);

    private final int status;

    private Messages(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
