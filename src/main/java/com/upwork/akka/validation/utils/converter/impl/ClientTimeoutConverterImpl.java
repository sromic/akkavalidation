package com.upwork.akka.validation.utils.converter.impl;

import com.google.inject.Singleton;
import com.upwork.akka.validation.actors.PerformanceActor;
import com.upwork.akka.validation.models.Log;
import com.upwork.akka.validation.utils.converter.ClientTimeoutConverter;

/**
 * Created by sromic on 01/09/16.
 */
@Singleton
public class ClientTimeoutConverterImpl implements ClientTimeoutConverter {

    @Override
    public Log convert(PerformanceActor.ClientTimedOut clientTimedOut) {
        final Log log = new Log();
        log.setResponseDuration(clientTimedOut.getProcessDuration());
        log.setClientId(clientTimedOut.getRequestId());

        return log;
    }
}
