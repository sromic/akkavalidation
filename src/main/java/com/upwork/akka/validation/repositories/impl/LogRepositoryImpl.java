package com.upwork.akka.validation.repositories.impl;

import com.avaje.ebean.annotation.Transactional;
import com.google.inject.Singleton;
import com.upwork.akka.validation.models.Log;
import com.upwork.akka.validation.repositories.LogRepository;

import java.util.List;

/**
 * Created by sromic on 01/09/16.
 */
@Singleton
@Transactional
public class LogRepositoryImpl implements LogRepository {

    @Override
    public void insert(Log model) {
        Log.finder.db().insert(model);
    }

    @Override
    public void delete(Log model) {
        Log.finder.db().delete(model);
    }

    @Override
    public void update(Log model) {
        Log.finder.db().update(model);
    }

    @Override
    public Log get(Long id) {
        return Log.finder.db().find(Log.class, id);
    }

    @Override
    public List<Log> getAll() {
        return Log.finder.all();
    }

}
