package com.upwork.akka.validation.repositories;

import com.upwork.akka.validation.models.Log;

/**
 * Created by sromic on 01/09/16.
 */
public interface LogRepository extends GenericRepository<Log> {
}
