package com.upwork.akka.validation.repositories;

import java.util.List;

/**
 * Created by sromic on 01/09/16.
 */
public interface GenericRepository<T> {
    void insert(T model);

    void delete(T model);

    void update(T model);

    T get(Long id);

    List<T> getAll();
}
