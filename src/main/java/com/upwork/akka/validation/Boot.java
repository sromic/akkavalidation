package com.upwork.akka.validation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.upwork.akka.validation.actors.ClientActor;
import com.upwork.akka.validation.actors.PerformanceActor;
import com.upwork.akka.validation.actors.ServerActor;
import com.upwork.akka.validation.injector.ComponentInjector;
import com.upwork.akka.validation.producer.DependencyInjector;
import scala.concurrent.duration.Duration;

import java.util.stream.IntStream;

/**
 * Created by sromic on 18/08/16.
 */
public final class Boot {
    public static void main(String... args) {
        final Injector injector = Guice.createInjector(new ComponentInjector());

        final ActorSystem actorSystem = ActorSystem.create("validation");

        final ActorRef performanceActor = actorSystem.actorOf(Props.create(DependencyInjector.class, injector, PerformanceActor.class), "performance");
        //final ActorRef clientActor = actorSystem.actorOf(ClientActor.props(performanceActor), "client1");
        final ActorRef serverActor = actorSystem.actorOf(ServerActor.props(performanceActor), "server1");
        //final ActorRef printingActor = actorSystem.actorOf(Props.create(DependencyInjector.class, injector, PrintingActor.class), "printing");

        //creating client actors and sending messages to serverActor as queue
        //serverActor is sending back response to clientActor and sending total processing time to
        // performanceActor who is inserting given data to db
        IntStream.range(1, 10000).forEach(i -> {
            final ActorRef clientActor = actorSystem.actorOf(ClientActor.props(performanceActor), "client" + i);
            serverActor.tell(ServerActor.Event.createInstance(clientActor.path().name()), clientActor);
        });

       /* actorSystem.scheduler().schedule(
                Duration.create(1, TimeUnit.SECONDS),
                Duration.create(5, TimeUnit.SECONDS),
                printingActor,
                PrintingActor.Print.START,
                actorSystem.dispatcher(),
                null);*/

        actorSystem.awaitTermination(Duration.Inf());
    }
}
