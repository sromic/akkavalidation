package com.upwork.akka.validation.provider;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.google.inject.Provider;


/**
 * Created by sromic on 18/08/16.
 */
public final class EbeanServerProvider implements Provider<EbeanServer> {

    @Override
    public EbeanServer get() {
        // EbeanServer configured by ebean.properties
        return Ebean.getDefaultServer();
    }
}
