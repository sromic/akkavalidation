create table log (
  id                            bigint auto_increment not null,
  client_id                     varchar(255),
  response_duration             integer,
  constraint pk_log primary key (id)
);

